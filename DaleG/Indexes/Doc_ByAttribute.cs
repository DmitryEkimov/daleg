﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DaleG.Models;
using Raven.Client.Indexes;

namespace DaleG.Indexes
{
    public class Doc_ByAttribute: AbstractIndexCreationTask<Doc>
    {
        public Doc_ByAttribute()
        {
            Map = Docs =>
                from p in Docs
                select new
                {
                    _ = p.Attributes.Select(attribute =>
                      CreateField(attribute.Property, attribute.Value, false, true))
                };
        }
    }
} 