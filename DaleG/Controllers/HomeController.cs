﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaleG.Models;
using Newtonsoft.Json;
using Raven.Client;

namespace DaleG.Controllers
{
    public class HomeController : RavenController
    {
        //
        // GET: /Home/

        [HttpGet]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Index(string[] Property, string[] Value)
        {
            RavenQueryStatistics stats;
            var query = Session.Advanced.LuceneQuery<Doc>("Doc/ByAttribute").Statistics(out stats);

            var count = Property.Length;
            for (int i = 0; i < count; i++)
            {
                query = query.AndAlso().WhereEquals(Property[i], Value[i]);
            }

            var docs = query.ToList().Select(d=>JsonConvert.SerializeObject(d,Formatting.None));
            return PartialView("_docSearchResult", docs);
        }

    }
}
