﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using DaleG.Indexes;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;

namespace DaleG.Controllers
{
    public class RavenController : Controller
    {
        public new IDocumentSession Session { get; set; }

        protected IDocumentStore _documentStore;

        public RavenController()//IDocumentStore documentStore
        {
            _documentStore = new DocumentStore() {ConnectionStringName = "CLOUDBIRD_CONNECTION_STRING"};
            _documentStore.Initialize();
            IndexCreation.CreateIndexes(typeof(Doc_ByAttribute).Assembly, _documentStore);//Assembly.GetCallingAssembly()
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Session = _documentStore.OpenSession();
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.IsChildAction)
                return;

            using (Session)
            {
                if (Session != null && filterContext.Exception == null)
                {
                    Session.SaveChanges();
                }
            }
            base.OnActionExecuted(filterContext);
        }
    }
}
