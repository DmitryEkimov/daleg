/// <reference path="jquery-1.8.d.ts"/>
/// <reference path="knockout.d.ts"/>
/// <reference path="knockoutmapping-2.0.d.ts"/>

// Module
module app.viewModels {

    // Class
    export class PropertyParam {
        Prop: any;
        Val= ko.observable('');
        constructor ( initialParams, val ) {
            this.Prop = initialParams;    
            this.Val = ko.observable(val);
                
        }
    }

    export class dalegViewModel {
        props = ko.observableArray([]);
        availableProperties = [
            "Profession" ,
            "Sports" ,
            "Interests" ,
            "Countries Visited" 
        ];
        self :any;
        constructor () {
            this.props = ko.observableArray([
            new PropertyParam("Profession", "Accountant"),
            new PropertyParam("Interests", "Travel"),
            new PropertyParam("Sports", "Golf")
             ]);
        }
        addProperty(): void {
            this.props.push(new PropertyParam(this.availableProperties[0], ""));
        };
        removeProperty(prop:PropertyParam) { 
            this.props.remove(prop);
        };
    }
}

$(document).ready(() => {
    // Local variables
    var ViewModel = new app.viewModels.dalegViewModel;
    ko.applyBindings(ViewModel);
    
});