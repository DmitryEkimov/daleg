var app;
(function (app) {
    (function (viewModels) {
        var PropertyParam = (function () {
            function PropertyParam(initialParams, val) {
                this.Val = ko.observable('');
                this.Prop = initialParams;
                this.Val = ko.observable(val);
            }
            return PropertyParam;
        })();
        viewModels.PropertyParam = PropertyParam;
        var _this;
        var dalegViewModel = (function () {
            function dalegViewModel() {
                _this = this;
                this.props = ko.observableArray([]);
                this.availableProperties = [
                    "Profession", 
                    "Sports", 
                    "Interests", 
                    "Countries Visited"
                ];
                this.props = ko.observableArray([
                    new PropertyParam("Profession", "Accountant"), 
                    new PropertyParam("Interests", "Travel"), 
                    new PropertyParam("Sports", "Golf")
                ]);
            }
            dalegViewModel.prototype.addProperty = function () {
                this.props.push(new PropertyParam(this.availableProperties[0], ""));
            };
            dalegViewModel.prototype.removeProperty = function (prop) {
                _this.props.remove(prop);
            };
            return dalegViewModel;
        })();
        viewModels.dalegViewModel = dalegViewModel;        
    })(app.viewModels || (app.viewModels = {}));
    var viewModels = app.viewModels;

})(app || (app = {}));

$(document).ready(function () {
    var ViewModel = new app.viewModels.dalegViewModel();
    ko.applyBindings(ViewModel);
});
