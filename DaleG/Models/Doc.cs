﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaleG.Models
{
    public class Doc
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Attribute[] Attributes { get; set; }
    }

    public class Attribute
    {
        public string Property { get; set; }
        public string[] Value { get; set; }
    }


            //    Doc doc = Session.Load<Doc>(1);

            //Session.Store(new Doc()
            //{
            //    FirstName = "Fred",
            //    LastName = "Smith",
            //    Attributes = new[]
            //                                                                                  {
            //                                                                                      new DaleG.Models.Attribute(){ Property = "Profession", Value = new string[] { "Accountant" }}, 
            //                                                                                      new DaleG.Models.Attribute(){ Property = "Sports", Value = new string[] { "Football","Golf","Rugby" }}, 
            //                                                                                      new DaleG.Models.Attribute(){ Property = "Interests", Value = new string[] { "Photography","Travel" }}, 
            //                                                                                      new DaleG.Models.Attribute(){ Property = "Countries Visited", Value = new string[] { "United Kingdom","France","USA" }}, 
            //                                                                                  }
            //});
}